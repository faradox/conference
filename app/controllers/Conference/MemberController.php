<?php

namespace App\Conference;

class MemberController extends \BaseController {

    public function index() {

        $query = ( new Member())
                ->select(['*'])
                ->query();

        $this->view("conference/members/list", ['data' => $query]);
    }

    public function display($id) {

        $query = ( new Member())
                ->select(['*'])
                ->where(['id' => $id])
                ->query();

        $this->view("conference/members/display", ['data' => $query]);
    }

    public function create() {

        if (!auth()) {
            $this->redirect('/auth/login');
        }
        $this->view("conference/members/partials/create");
    }

    public function store() {
        if (!auth()) {
            $this->redirect('/auth/login');
        }

        $name = $this->request->getParam("name");
        $email = $this->request->getParam("email");
        $age = $this->request->getParam("age");
        $occ = $this->request->getParam("occupation");
        $status = $this->request->getParam("status");


        $input = [
            'name' => $name ?: 'NULL',
            'email' => $email ?: 'NULL',
            'age' => $age ?: 'NULL',
            'occupation' => $occ ?: 'NULL',
            'status' => $status ?: 0
        ];

        try {
            $stmt = ( new Member())
                    ->insert($input);
            $this->redirect('/');
        } catch (\PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function edit($id) {

        if (!auth()) {
            $this->redirect('/auth/login');
        }

        $query = ( new Member())
                ->select(['*'])
                ->where(['id' => $id])
                ->query();

        $this->view("conference/members/partials/edit", ['member' => $query[0]]);
    }

    public function update($id) {
        if (!auth()) {
            $this->redirect('/auth/login');
        }

        $name = $this->request->getParam("name");
        $email = $this->request->getParam("email");
        $age = $this->request->getParam("age");
        $occ = $this->request->getParam("occupation");
        $status = $this->request->getParam("status");

        $input = [
            'name' => $name ?: 'NULL',
            'email' => $email ?: 'NULL',
            'age' => $age ?: 'NULL',
            'occupation' => $occ ?: 'NULL',
            'status' => $status ?: 0
        ];

        try {
            $stmt = ( new Member())
                    ->update($input, ['id' => $id]);
            $this->redirect('/');
        } catch (\PDOException $ex) {
            echo $ex->getMessage();
        }
    }

}
