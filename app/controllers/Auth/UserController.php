<?php

namespace App\Auth;

class UserController extends \BaseController {

    public function login() {
        $this->layout = 'auth';
        if (auth()) {
            $this->redirect('/');
        } else {
            $this->view('auth/login');
        }
    }

    public function doLogin() {

        $username = $this->request->getParam("username");
        $password = $this->request->getParam("password");

        if ($username && $password) {

            $user = new User();

            if ($user->authenticate($username, $password)) {
                $this->goBack();
            } else {
                $this->redirect('/auth/login');
            }
        }
    }

    public function logout() {
        session_destroy();
        $this->redirect('/');
    }

}
