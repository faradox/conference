<?php

namespace App\Auth;

class User extends \Core\Database\Model {

    protected $table = 'users';

    public function authenticate($login, $password) {

        $query = $this->select(['id', 'name',
                    'password_hash'
                ])
                ->where(['username' => $login])
                ->limit(1)
                ->query();
        
        

        if (!empty($query)) {
            if (password_verify($password, $query[0]['password_hash'])) {
                $this->setUser($query);
                return true;
            } else {
                return false;
            }
        }
    }

    public function setUser($data) {
        $_SESSION['id'] = $data[0]['id'];
        $_SESSION['name'] = $data[0]['name'];
    }

    public function checkAuth() {
        return isset($_SESSION['id']) ? true : false;
    }

}
