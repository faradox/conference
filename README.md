# Web application


## Requirements

- [PHP](http://www.php.net) >= 5.6

## Installation

You can install using 
```bash
$ git clone git clone https://faradox@bitbucket.org/faradox/conference.git
```
Then

```bash
$ composer install
```

Document root  /web

login: admin
password: pass 

## Database
sql dump file exists inside project
