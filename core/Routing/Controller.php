<?php

namespace Core\Routing;

abstract class Controller {

    protected $layout;
    
    protected $request;

    public function __construct() {
        $this->request = new \Core\Http\Request();
    }

    public function view($view = null, $data = []) {

        $viewPath = __DIR__ . '/../../resources/views/';

        if (file_exists($viewPath . $view . '.php')) {
            $layoutPath = __DIR__ . '/../../resources/views/layouts/';
            $output = $this->getView($viewPath . $view . '.php', $data, true);
            $output = $this->getView($layoutPath . $this->layout . '.php', ["body" => $output]);
        } else {
            throw new \Exception("{$view} not found");
        }
    }

    protected function getView($view, $data, $return = false) {
        extract($data);
        ob_start();
        include $view;
        $renderedView = ob_get_clean();

        if ($return) {
            return $renderedView;
        } else {
            echo $renderedView;
        }
    }
    
    public function redirect($route,$statusCode = 302) {
        header('Location: '.$route, true, $statusCode);
        exit();
    }

    public function goBack() {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

}
