<?php

function auth() {
	$user = new \App\Auth\User();
	return $user->checkAuth();
}