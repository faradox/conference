<?php

$router = new \Bramus\Router\Router();

$router->set404(function() {
    echo "Not found";
});
// $router->before('GET|POST', '/members/create', function() {
//     if (!isset($_SESSION['name'])) {
//         header('location: /auth/login');
//         exit();
//     }
// });

// $router->before('GET', '/members/edit/.*', function() {
//     if (!isset($_SESSION['name'])) {
//         header('location: /auth/login');
//         exit();
//     }
// });

// $router->before('POST', '/members/update/.*', function() {
//     if (!isset($_SESSION['name'])) {
//         header('location: /auth/login');
//         exit();
//     }
// });

$router->get('/', '\App\Conference\MemberController@index');

$router->get('/auth/login', '\App\Auth\UserController@login');

$router->get('/logout', '\App\Auth\UserController@logout');

$router->post('/auth/login', '\App\Auth\UserController@doLogin');




$router->get('/members/get/(\d+)', '\App\Conference\MemberController@display');

$router->get('/members/create', '\App\Conference\MemberController@create');

$router->get('/members/edit/(\d+)', '\App\Conference\MemberController@edit');

$router->post('/members/update/(\d+)', '\App\Conference\MemberController@update');

$router->post('/members/create', '\App\Conference\MemberController@store');

$router->run();
