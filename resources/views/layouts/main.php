<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Список участников</title>
    </head>
    <body>
        <?php if(auth()):?>
        <p>Вы зашли как <?php echo $_SESSION['name'];?> <a href="/logout">Выйти</a></p>
        <?php else:?>
        <a href="/auth/login">Авторизация</a>
        <?php endif?>
        <?php echo $body; ?>

    </body>
</html>