<?php if (!empty($data)) : ?>
    <?php foreach ($data as $member): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $member['name']; ?></h3>
            </div>
            <div class="panel-body">
                <p>Email: <?php echo $member['email']; ?></p>
                <p>Возрасть: <?php echo $member['age']; ?></p>
                <p>Род заянтий: <?php echo $member['occupation']; ?></p>
                <p>Статус: <?php echo $member['status'] == 1 ? 'Был' : 'Не был'; ?></p>
            </div>
        </div>
    <?php endforeach; ?>
<a href="/">Вернуться</a>
<?php else: ?>
    <div class="alert alert-danger" role="alert">Ошибка запроса</div>
<?php endif; ?>