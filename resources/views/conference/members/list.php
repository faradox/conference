<a href="/members/create">Добавиь участника</a>
<?php if (!empty($data)) : ?>
    <?php foreach($data as $member): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $member['name']; ?></h3>
        </div>
        <div class="panel-body">
            <p>Email: <?php echo $member['email'];?></p>
            <p>Age: <?php echo $member['age'];?></p>
            <a href="members/get/<?php echo $member['id']?>">Посмотреть профиль</a>
            <a href="members/edit/<?php echo $member['id']?>">Редактировать</a>
        </div>
    </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="alert alert-danger" role="alert">Не существут ни одного участника</div>
<?php endif; ?>
