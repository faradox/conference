<style>
    .member-form {
        width: 250px;
    }
</style>
<div class="member-form">
    <form method="post" action="/members/update/<?php echo $member['id'] ?>">
        <input type="text" name="name" placeholder="Имя" value="<?php echo $member['name'] ?>"/>
        <input type="email" name="email" placeholder="Email" value="<?php echo $member['email'] ?>"/>
        <input type="text" name="occupation" placeholder="Род занятий" value="<?php echo $member['occupation'] ?>"/>
        <input type="text" name="age" placeholder="Возрасть" value="<?php echo $member['age'] ?>"/>
        <input type="radio" name="status" value="1" <?php echo $member['status'] == 1 ? 'checked' : ''; ?>>Был<br>
        <input type="radio" name="status" value="0" <?php echo $member['status'] == 0 ? 'checked' : ''; ?>> Не был<br>
        <input type="submit" value="Сохранить" />
    </form>
</div>
<a href="/">Главная</a></p>