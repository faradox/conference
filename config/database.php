<?php

return [
    'default' => 'mysql',
    'databases' => [
        'mysql' => [
            'dsn' => 'mysql:host=localhost;dbname=conference',
            'username' => 'root',
            'password' => 'admin',
            'charset' => 'utf8'
        ]
    ]
];
